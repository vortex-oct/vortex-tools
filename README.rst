Vortex Tools
============

`vortex-oct-tools` is a package of Python utility functions for `vortex`, primarily for inspecting and analyzing scan patterns anr markers.
See the `website <https://www.vortex-oct.dev/rel/latest/doc/>`_ and for more information about `vortex`.
